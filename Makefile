PRE=sc_review

all:
#	pdflatex $(PRE)
	latex $(PRE)
	dvipdf $(PRE).dvi

full:
#	pdflatex $(PRE)
	latex $(PRE)
	bibtex $(PRE)
	latex $(PRE)
	latex $(PRE)
	dvipdf $(PRE)

read:
	evince $(PRE).pdf
